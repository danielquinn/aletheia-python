from .base import ImageFile


class PngFile(ImageFile):

    SUPPORTED_TYPES = ("image/png",)
