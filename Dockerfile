#
# This "run Aletheia in a container" process very beta, so input/suggestions
# are appreciated.  If you're new to this project, it may just be best to
# install Aletheia the normal way.
#
# This is just a simple way to run Aletheia without having to install exiftool
# & ffmpeg on your local system.  However, as Aletheia operates directly on
# files, using this container introduces additional complexity as you need to
# mount the directory containing your target file(s) as a shared volume.
#
# For example, if you have a file in `/tmp/for-verification`, you need to
# mount that path as a volume for Aletheia to be able to see & verify it:
#
#     $ docker run \
#       --rm \
#       -v /tmp/for-verification:/tmp/for-verification \
#       -t danielquinn/aletheia \
#       aletheia verify /tmp/for-verification/file.jpg
#
# Additionally, if you want Aletheia to sign stuff, you'll probably want to
# mount a path on your host machine as Aletheia's config directory, lest you
# lose your private key as soon as the docker instance is killed:
#
#    $ docker run \
#      --rm \
#      -v /home/my_user/.config/aletheia:/home/app/.config/aletheia \
#       -v /tmp/for-signing:/tmp/for-signing \
#       -t danielquinn/aletheia \
#       aletheia sign /tmp/for-signing/file.jpg
#

FROM python:3-alpine

RUN apk add --update --no-cache bash ffmpeg exiftool libffi-dev libmagic
RUN pip install --upgrade pip

COPY . /app

RUN apk add --update --no-cache --virtual .build-deps gcc g++ make tzdata ca-certificates python3-dev musl-dev openssl-dev cargo \
  && pip install /app/ \
  && apk del .build-deps \
  && rm -rf /root/.cache

CMD /usr/local/bin/aletheia

